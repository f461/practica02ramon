<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Entradas;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;


class SiteController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionListar() {
        
        $a=Entradas::find()  // ActiveQuery
                ->all();  // Array de modelos generados a traves de los active record;
        
        
        $b=Entradas::find()
                ->asArray() // crea un array de arrays asociativos
                ->all();
        return $this->render("ListarTodos",[
            "datos" => $b,
             
        ]);
        
    }
    
    
    public function actionListar1() {
        $b=Entradas::find()->all();
        
        return $this->render("ListarTodos",[
            "datos"=>$b,
            
            
        ]);
        
        
    }
    
    public function actionListar2() {
        $b=Entradas::find()
                ->select(['texto'])
                ->asArray()
                ->all (); //esto es un array de datos
               
        
        return $this->render("ListarTodos",[
            "datos"=>$b,
            
            
        ]);
        
        
    }
    
    
      public function actionListar3() {
        $b=Entradas::find()
                ->select(['texto'])
                ->all (); // array de modelos
               
        
        return $this->render("ListarTodos",[
            "datos"=>$b,
            
            
        ]);
        
        
    }
    
      public function actionListar4() {
          
          $salida=new Entradas (); //modelo basado en active Record vacio.
          
             
        
        return $this->render("ListarTodos",[
            "datos"=> $salida->find()->all(),
            
            
        ]);
        
        
    }
    
    
     public function actionListar5() {
          
          $salida=new Entradas (); //modelo basado en active Record vacio.
          
             
        
        return $this->render("ListarTodos",[
            "datos" => $salida->findone(1),
           // "datos" => Entradas::findone(1),
           //"datos" => Entradas::find()-where("id=1")->one()
        ]);
        
        
    }
    
    
    
    
      public function actionListar6() {
         
        return $this->render("ListarTodos",[
            "datos"=> tii::$app->db->createCommand("select * form entradas")->queryAll(),

        ]);
        
        
    }
    
    public function actionMostar(){
        $dataProvider=new ActiveDataProviders([
            "query" =>Entradas::find(),
            
            
            
        ])
        return $this->render ('mostrar'),[
            ""
            
            
        ])
        
    }
    
    public function actionmostraruno(){
        return $this->render ("mostaruno"),[
            'model'=> Entradas::findOne(1)
            
            
        ]);
        
        
    }
    
    //consulta en yii
    public function actionConsulta1() {
        // mostrar todos los textos de la tabla entradas
        
        // voya realizar la consulta utilizando create comannda y sql
        
       $salida=Yii::$app->db->createCommand("select texto from entradas"); // clase comando sql 
       $salida1=$salida->queryAll(); //array con todos los registros
//var_dump($salida);
                
        //var_dump($salida1);
       
       return $this->render("vistaConsulta1",[
           "registros" => $salida1
       ]);
    }
    
    
    public function actionConsulta2 (){
        //Mostar todos los id de la tabla entradas
        // voya realizar la consulta utilizando create comannda y sql
        $salida=Yii::$app->db->createCommand("select id from entradas");
        $salida1=$salida->queryAll();
        
        return $this->render("vistaConsulta2",[
           "registros" => $salida
       ]);
    }
        public function actionConsulta3(){
            //voy a realizar la consulta con activeRecord
            
           $salida=Entradas::find()->select("texto"); //esto es un activeQuery
           $salida1=$salida->all(); //array de activerecords
           
            return $this->render("vistaConsulta3",[
                "registros" => $salida1

            ]);
        }
    
    public function actionConsulta4(){
        //mostrar todo los id de la tabla entradas
        
         $salida=Entradas::find()->select("id")->all();
         
         
          return $this->render("vistaConsulta4",[
                "registros" => $salida

            ]);
        }
    

    public function actionConsulta5(){
        //todos los datos de la tabla entradas
        //create command
        $salida=Yii::$app->db->createCommand("select * from entradas")->queryAll();
        
       
        return $this->render("vistaConsulta5",[
           "registros" => $salida
       ]);
    }
        




public function actionConsulta6() {
        //todos los datos de la tabla entradas
        //active record
    
    $salida=Entradas::find()->all();
         
         
          return $this->render("vistaConsulta6",[
                "registros" => $salida,
                "modelo"=> new Entradas(),

            ]);
        }
        
        
public function actionConsulta7(){
//listar todos los registros de la tabla con un gridview        

    
        $consultaActiva=Entradas::find(); // activeQuery 
        
        //creo un activeDataProvider con la ActiveQuery
        $dataProvider = new ActiveDataProvider([
            'query' => $consultaActiva,
            ]);
            return $this->render("VistaConsulta7",[
                   
                'datos' => $dataProvider,
            ]);
                
}


//listar todos los registro de la tabla PERO SOLO EL CAMPO TEXTO con gridview


    public function actionConsulta8() {
        
        $consultaActiva= Entradas::find();
        
        $dataProvider = new ActiveDataProvider([
            'query'=> $consultaActiva,
            ]);
            return $this->render("VistaConsulta8",[
                   
                'datos' => $dataProvider,
            ]);
            
        
        
    }
    
    //listar todos los registros 
    

    public function actionConsulta9() {
        //Mostrar todas las entradas
        // a traves de un listview
        
        $dataProvider = new ActiveDataProvider([
            'query' => Entradas::find(),

        ]);
        
        return $this->render("vistaConsulta9",[
                   
                'datos' => $dataProvider,
        
        ]);
    }
    
//la misma consulta 8 pero con listview 
    
    public function actionConsulta10() {
        
        $consultaActiva= Entradas::find()->select('texto');
        
        $dataProvider = new ActiveDataProvider([
            'query'=> $consultaActiva,
            ]);
            return $this->render("VistaConsulta10",[
                   
                'datos' => $dataProvider,
            ]);
    }
        
        public function actionConsulta11(){
            
            //mostrar el texto de la noticia con id =
            // utilizando un detailview
            $registroActivo= Entradas::fin()->select("texto")->where("id=1")->one();
           
            return $this->render("VistaConsulta11",[
                "modelo"=> $registroActivo,
                
                
            ]);
                    
                    
        

        }
        
        // mostra todos lso registros de entrada utilizando sqldataprovider y gridview
        
        public function actionConsulta12(){
            //mostrar todos lore registros de  entrada con sqldataprovider y gridview
            $datos=new sqlDataProvider([
                'sql' => 'select * from entradas',
                
                
            ]);
            
            return $this-> render("vistaConsulta12",[
                'datos' => $datos,

            ]);
            
        }
}
    
    
 