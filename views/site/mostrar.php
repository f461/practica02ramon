<?php
use yii\grid\GridView;

echo GridView::widget([
    'dataprovider'=> $dataProvider,
    'summary' => "Mostrando {begin} - {end} de {totalcount} elementos",
    'columns' =>[
        ['class' => 'yii\grid\SerialColumn'],
        'id',
        'texto',
        
    ]
]);
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

